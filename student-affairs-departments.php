<?php

/**
 * Plugin Name:       UA Student Affairs - Departments
 * Plugin URI:        http://sa.ua.edu/
 * Description:       @TODO This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0
 * Author:            Rachel Carden, UA Student Affairs
 * Author URI:        http://sa.ua.edu/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       ua-sa-departments
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// Add files
require_once plugin_dir_path( __FILE__ ) . 'includes/filters.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/query.php';

// Add admin functionality in admin only
if( is_admin() ) {
	require_once plugin_dir_path( __FILE__ ) . 'includes/admin.php';
}

// Internationalization
// @TODO Add language files
/*add_action( 'init', 'ua_sa_departments_textdomain' );
function ua_sa_departments_textdomain() {
	load_plugin_textdomain( 'ua-sa-departments', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}*/

// Runs on install
register_activation_hook( __FILE__, 'ua_sa_departments_install' );
function ua_sa_departments_install() {

	// Register the custom post type
	ua_sa_departments_register_cpt();

	// Flush the rewrite rules to start fresh
	flush_rewrite_rules();

}

// Runs when the plugin is upgraded
// @TODO test to see if this only runs when running a bulk upgrade
add_action( 'upgrader_process_complete', 'ua_sa_departments_upgrader_process_complete', 1, 2 );
function ua_sa_departments_upgrader_process_complete( $upgrader, $upgrade_info ) {

	// Flush the rewrite rules to start fresh
	flush_rewrite_rules();

}

// Register departments custom post type
add_action( 'init', 'ua_sa_departments_register_cpt' );
function ua_sa_departments_register_cpt() {
	global $blog_id;

	// Only register on main site
	if ( ! $blog_id || ! is_main_site( $blog_id ) ) {
		return false;
	}

	// Register departments CPT
	register_post_type( 'departments', array(
		'labels' => array(
			'name'               => _x( 'Departments', 'post type general name', 'ua-sa-departments' ),
			'singular_name'      => _x( 'Department', 'post type singular name', 'ua-sa-departments' ),
			'menu_name'          => _x( 'Departments', 'admin menu', 'ua-sa-departments' ),
			'name_admin_bar'     => _x( 'Departments', 'add new on admin bar', 'ua-sa-departments' ),
			'add_new'            => _x( 'Add New', 'departments', 'ua-sa-departments' ),
			'add_new_item'       => __( 'Add New Department', 'ua-sa-departments' ),
			'new_item'           => __( 'New Department', 'ua-sa-departments' ),
			'edit_item'          => __( 'Edit Department', 'ua-sa-departments' ),
			'view_item'          => __( 'View Department', 'ua-sa-departments' ),
			'all_items'          => __( 'All Departments', 'ua-sa-departments' ),
			'search_items'       => __( 'Search Departments', 'ua-sa-departments' ),
			'parent_item_colon'  => __( 'Parent Department:', 'ua-sa-departments' ),
			'not_found'          => __( 'No departments found.', 'ua-sa-departments' ),
			'not_found_in_trash' => __( 'No departments found in Trash.', 'ua-sa-departments' )
		),
		'public'                => true,
		'publicly_queryable'    => true,
		'exclude_from_search'   => false,
		'show_in_nav_menus'     => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'show_in_admin_bar'     => true,
		'menu_icon'             => 'dashicons-networking',
		'capabilities'          => array(
			'edit_post'         => 'edit_department',
			'read_post'         => 'read_department',
			'delete_post'       => 'delete_department',
			'edit_posts'        => 'edit_departments',
			'edit_others_posts' => 'edit_others_departments',
			'publish_posts'     => 'publish_departments',
			'read_private_posts'=> 'read_private_departments',
			'read'              => 'read',
			'delete_posts'      => 'delete_departments',
			'delete_private_posts' => 'delete_private_departments',
			'delete_published_posts' => 'delete_published_departments',
			'delete_others_posts' => 'delete_others_departments',
			'edit_private_posts' => 'edit_private_departments',
			'edit_published_posts' => 'edit_published_departments',
			'create_posts'      => 'edit_departments'
        ),
		'hierarchical'          => false,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions' ),
		'has_archive'           => false,
		'rewrite'               => array(
			'slug'  => 'departments',
			'feeds' => false,
			'pages' => true,
		),
		'query_var'             => true,
	) );

}

