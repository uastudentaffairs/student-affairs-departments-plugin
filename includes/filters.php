<?php

// Manage some filters
add_action( 'pre_get_posts', function( $query ) {

	// Don't trim the excerpt on the departments page
	// Can't put this in the departments template file
	// because then it wouldn't trigger for the API request
	// Have to check single array with json queries
	$post_type = $query->get( 'post_type' );
	if ( 'departments' == $post_type
	     || ( is_array( $post_type ) && in_array( 'departments', $post_type ) && count( $post_type ) == 1 ) ) {

		remove_filter( 'get_the_excerpt', 'wp_trim_excerpt' );

	}

});