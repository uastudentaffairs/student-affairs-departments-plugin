<?php

add_action( 'pre_get_posts', function( $query ) {

	// Always order departments by name ASC
	// Have to check single array with json queries
	$post_type = $query->get( 'post_type' );
	if ( 'departments' == $post_type
		|| ( is_array( $post_type ) && in_array( 'departments', $post_type ) && count( $post_type ) == 1 ) ) {

		$query->set( 'orderby' , 'title' );
		$query->set( 'order' , 'ASC' );

	}

}, 100 );